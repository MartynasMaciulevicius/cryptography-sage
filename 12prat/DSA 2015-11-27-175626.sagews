︠9fd06fe8-d91f-49cf-a013-3c9c0435e1bd︠
A='abcdefghijklmnopqrstuvwxyz0123456789'
def hash(text,n):
    t=''
    for r in text:
        if r in A:
            ind=A.index(r)+1
            if ind<10: t=t+'0'+str(ind)
            else: t=t+str(ind)
    return int(t,10)%n  

hashed = hash("Martynas Maciulevicius",100000)
hashed
︡2790ee75-8f64-47b4-9937-2a02813ae6f2︡︡{"stdout":"92119\n","done":false}︡{"done":true}
︠5ff635c9-5e9c-4d4b-9cf1-077f782367c1︠

p = next_prime(325^24)
p
︡768afa49-4d60-4b36-a7f5-9d7fc57c39e4︡︡{"stdout":"1928415721772475822503151338338511777692474424839019775390691\n","done":false}︡{"done":true}
︠5ab2a271-791c-4b85-9c0d-adaad0043baei︠
factors = factor(p - 1)
factors
︡c247c7ec-d564-448d-977b-7519c66354d6︡︡{"stdout":"2 * 3 * 5 * 13 * 43 * 73 * 829 * 1900160169044370814498189948960375586290531455941341\n","done":false}︡{"done":true}
︠b8a053c1-5d13-4f01-b6a3-f4a60922ab26︠
g = primitive_root(p)
g
︡ed379589-d2b1-487c-a1ac-7f302d0a1543︡︡{"stdout":"2\n","done":false}︡{"done":true}
︠e50bee50-055c-4112-9b41-83ae18b95900︠
q = 1900160169044370814498189948960375586290531455941341
︡8c539a74-1171-4cdd-8f59-9256676882de︡︡{"done":true}
︠d78d8e29-1505-4c62-af64-833cf8771c43︠
alpha = power_mod(g, (p - 1) // q, p)
alpha
︡72fd9655-679c-474a-a570-c658a5455749︡︡{"stdout":"1041142729833493350241449759724005465515724379694511662178764\n","done":false}︡{"done":true}
︠f8b3d143-dfee-4b7e-97a5-301f6a7a1d48︠

power_mod(alpha, q, p) == 1
︡57dffa5c-06de-4a75-8018-14e9d58c57c9︡︡{"stdout":"True\n","done":false}︡{"done":true}
︠fa74cc04-0f73-4cf9-b800-2bcccfd1e261︠

a = int(random() * (25^11 + 254)) % (q - 1)
a
︡cd1d16f8-3ad3-45d3-822b-2c9b28d66f24︡︡{"stdout":"1171325931176819\n","done":false}︡{"done":true}
︠b6aa48f9-d846-4d26-871d-f8ec18251fff︠

beta = power_mod(alpha, a, p)
beta
︡1419011d-fc28-4147-8b27-a3d8cdb78c2b︡︡{"stdout":"899783363871563270396500778877169238697595060457089712977119\n","done":false}︡{"done":true}
︠9bfbb74c-9a8f-44d5-937e-e8015c345f80︠


key = (p, q, alpha, beta)
key
︡77584300-3b64-49c3-b0f4-963220f355f2︡︡{"stdout":"(1928415721772475822503151338338511777692474424839019775390691, 1900160169044370814498189948960375586290531455941341, 1041142729833493350241449759724005465515724379694511662178764, 899783363871563270396500778877169238697595060457089712977119)\n","done":false}︡{"done":true}
︠21723af3-7285-4368-925b-ec80d9f14358︠

k = int(random() * 412 * 15 ^ 15)
k
gcd(k, q)
︡e8f87ee5-af3f-4069-835d-4d6b6f24c1dc︡︡{"stdout":"10708333105568837632L\n","done":false}︡{"stdout":"1\n","done":false}︡{"done":true}
︠c7d9381e-f648-418e-808e-ed9a23008a9e︠
gamma = power_mod(alpha, k, p) % q
gamma
︡f0028723-d929-42f6-a47b-7cbd21bdd131︡︡{"stdout":"190315670141206070931513348387718912464424949621191\n","done":false}︡{"done":true}
︠0e222ef7-2058-4209-a9a3-467b8763bfd7︠
delta = (hashed + a * gamma) / k % q
delta
︡92fb40cf-6d27-405c-940d-2ae2e4e6abfb︡︡{"stdout":"52938128645736941126305721757955398318686184654081\n","done":false}︡{"done":true}
︠c50ef261-bd5c-428c-9bdb-43e4a00974f3︠

gcd(gamma, q)
︡8eb7ea15-bee0-4bf1-911f-8fdee9188536︡︡{"stdout":"1\n","done":false}︡{"done":true}
︠85bf50e5-a92c-4d8c-bdf7-58dde13c1ade︠
# verification
e_1 = hashed / delta % q
e_2 = gamma / delta % q
(e_1, e_2)
︡89b1764b-f56f-41c1-a3f9-944ad56c60c9︡︡{"stdout":"(1540384348526568654887113315060762695864513541058145, 1392684782267428410440716361362135436741018995552622)\n","done":false}︡{"done":true}
︠71910966-5f67-4e91-9cda-49f2e690b652︠

power_mod(alpha, e_1, p) * power_mod(beta, e_2, p) % p % q == gamma % q
︡5163aa81-ef91-4e58-87a6-3496135f71af︡︡{"stdout":"True\n","done":false}︡{"done":true}
︠017c5f9c-a2f8-4427-82ee-96da1804f067︠










