#### --------------------------------------------------------
#### By Aleksej Komarov
#### --------------------------------------------------------

import time

## --------------------------------------------------------
##  Functions
## --------------------------------------------------------

## Rotate list
def rotate(l, n):
    return l[n:] + l[:n]

## Convert number to binary
def uint8_to_bits(x):
    return str(bin(x % 256))[2:].zfill(8)

## Convert binary to number
def uint8_from_bits(x):
    return int(str(x), 2) % 256

## Uint8 rotate op
def uint8_rotate(x, n):
    x = uint8_to_bits(x)
    x = rotate(x, n)
    return uint8_from_bits(x)

## Add two Uint8 numbers with overflow
def uint8_add(a, b):
    return (a + b) % 256

## Subtract two Uint8 numbers with overflow
def uint8_sub(a, b):
    return (a - b) % 256


## --------------------------------------------------------
##  TEA algorithm
## --------------------------------------------------------

## Decode one pair
def tea_iterate_pair(key, pair, encode = True):
    x1 = uint8_add(
        pair[0],
        key[0]
    )
    x2 = uint8_add(
        uint8_rotate(pair[0], 2),
        key[1]
    )
    x3 = uint8_add(
        uint8_rotate(pair[0], -2),
        key[2]
    )
    if encode:
        return [uint8_add(pair[1], x1 ^ x2 ^ x3), pair[0]]
    else:
        return [uint8_sub(pair[1], x1 ^ x2 ^ x3), pair[0]]

def tea_encode_pair(key, pair):
    for i in key:
        pair = tea_iterate_pair(key, pair, True)
        key = rotate(key, 1)
    return rotate(pair, 1)

def tea_decode_pair(key, pair):
    for i in key:
        key = rotate(key, -1)
        pair = tea_iterate_pair(key, pair, False)
    return rotate(pair, 1)

def tea_decrypt_ecb(key, ciphertext):
    result = ''
    for pair in ciphertext:
        pair = tea_decode_pair(key, pair)
        result += chr(pair[0]) + chr(pair[1])
    return result

def tea_decrypt_cbc(key, ciphertext, iv):
    result = ''
    for pair in ciphertext:
        iv_iter = iv
        iv = pair
        pair = tea_decode_pair(key, pair)
        result += chr(pair[0] ^ iv_iter[0])
        result += chr(pair[1] ^ iv_iter[1])
    return result

def tea_decrypt_ofb(key, ciphertext, iv):
    result = ''
    for pair in ciphertext:
        iv = tea_encode_pair(key, iv)
        result += chr(pair[0] ^ iv[0])
        result += chr(pair[1] ^ iv[1])
    return result


## --------------------------------------------------------
##  Main
## --------------------------------------------------------

key = [97, 114, 103]
ciphertext = [[80, 31], [243, 86], [50, 75], [3, 193], [50, 75], [216, 82], [155, 106], [143, 92]]

print tea_decrypt_ecb(key, ciphertext)

key = [97, 114, 103]
iv = [ord('a'), ord('i')]
ciphertext = [[147, 20], [51, 98], [73, 209], [106, 243], [78, 164], [226, 255], [148, 210], [159, 98], [22, 71], [48, 201], [64, 77]]

print tea_decrypt_cbc(key, ciphertext, iv)

key = [97, 114, 103]
iv = [ord('n'), ord('e')]
ciphertext = [[230, 38], [62, 186], [189, 45], [173, 184], [68, 28], [172, 66], [64, 237], [180, 73], [56, 175], [43, 88], [125, 238], [185, 152], [199, 199], [73, 36], [251, 193], [16, 98], [100, 152]]
print tea_decrypt_ofb(key, ciphertext, iv)
